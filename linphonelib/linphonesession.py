# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os
import tempfile

from contextlib import contextmanager
from functools import wraps

import pexpect

from linphonelib.commands import (AnswerCommand,
                                  CallCommand,
                                  QuitCommand,
                                  HangupCommand,
                                  HoldCommand,
                                  HookStatusCommand,
                                  RegisterCommand,
                                  ResumeCommand,
                                  UnregisterCommand,
                                  TransferCommand)


def _execute(f):
    @wraps(f)
    def func(self, *args):
        return self._linphone_shell.execute(f(self, *args))

    return func


class Session(object):

    def __init__(self, uname, secret, hostname, local_sip_port, local_rtp_port, logfile=None):
        self._uname = uname
        self._secret = secret
        self._hostname = hostname
        self._linphone_shell = _Shell(local_sip_port, local_rtp_port, logfile)

    def __str__(self):
        return 'Session %(_uname)s@%(_hostname)s' % self.__dict__

    @_execute
    def answer(self):
        return AnswerCommand()

    @_execute
    def call(self, exten):
        return CallCommand(exten)

    @_execute
    def hangup(self):
        return HangupCommand()

    @_execute
    def hold(self):
        return HoldCommand()

    @_execute
    def hook_status(self):
        return HookStatusCommand()

    @_execute
    def register(self):
        return RegisterCommand(self._uname, self._secret, self._hostname)

    @_execute
    def resume(self):
        return ResumeCommand()

    @_execute
    def transfer(self, exten):
        return TransferCommand(exten)

    @_execute
    def unregister(self):
        return UnregisterCommand()


class _Shell(object):
    _DOCKER_IMG = "xivo-linphone"

    _CONFIG_FILE_CONTENT = '''\
[sip]
sip_port={sip_port}

[rtp]
audio_rtp_port={rtp_port}
'''

    def __init__(self, sip_port, rtp_port, logfile=None):
        self._sip_port = sip_port
        self._rtp_port = rtp_port
        self._process = None
        self._config_filename = ''
        self._logfile = logfile

    def __del__(self):
        if self._process:
            self.execute(QuitCommand())
            if self._process.isalive():
                self._process.terminate(force=True)

        if os.path.exists(self._config_filename):
            os.unlink(self._config_filename)

    def execute(self, cmd):
        if not self._process:
            self._start()

        return cmd.execute(self._process)

    def _start(self):
        if os.getenv('USE_DOCKER'):
            cmd = "docker run --rm -ti -v {linphonerc}:/root/.linphonerc {docker_image}"
        else:
            cmd = 'sh -c "linphonec -c {linphonerc}" &'

        config_file = self._create_config_file()

        self._process = pexpect.spawn(cmd.format(linphonerc=config_file,
                                                 docker_image=self._DOCKER_IMG))
        if self._logfile:
            self._process.logfile = self._logfile

    def _create_config_file(self):
        if self._config_filename:
            return self._config_filename

        content = self._CONFIG_FILE_CONTENT.format(sip_port=self._sip_port, rtp_port=self._rtp_port)

        with tempfile.NamedTemporaryFile(delete=False) as f:
            self._config_filename = f.name
            f.write(content.encode())

        return self._config_filename


@contextmanager
def registering(session):
    session.register()
    try:
        yield session
    finally:
        session.unregister()
