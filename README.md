pylinphonelib
=============
[![Build Status](https://travis-ci.org/xivo-pbx/pylinphonelib.png?branch=master)](https://travis-ci.org/xivo-pbx/pylinphonelib)

pylinphonelib is a python library to drive the linphonec application from python.


Requirements
------------

* linphonec

Docker
------

It is possible to use the xivo-linphone docker image to run linphonec. To enable this
feature, set the USE_DOCKER environment variable.